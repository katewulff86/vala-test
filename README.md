# `libverify`
A simple library for unit testing. It supports nested test suites, test cases,
and fixtures.

## Licence
`libverify` is released under the LGPL.

## Rationale
`libverify` has two main design goals: simplicity and ease of use. It only
includes features necessary for a unit testing framework, and no new features
will be added unless a compelling reason exists to add them.

## Compilation and installation
Add it as a meson subproject dependency. To do so, create the following
`libverify.wrap` file in your `subprojects` folder:

```wrap
[wrap-git]
url = https://gitlab.com/katewulff86/libverify.git
revision = head
```
and then in your `meson.build` add
```meson
libverify_dep = dependency('libverify', fallback: ['libverify', 'libverify_dep'])
```
`libverify_dep` can then be added to your test binary's dependencies.

You can see this project's `meson.build` and `test` directory as an example of
how this works.

## Usage
### Running tests
An individual test case or test suite can be run by calling `my_test.run ()`
This will return an object which subclasses `Verify.Result`. This result can
be reported with a `Verify.Report` object. For example,
```vala
var my_test = new MyTest ();
var result = my_test.run ();
var reporter = new DefaultReporter ();
reporter.report (my_test.run ())
```
While this can be done manually, there is also `Runner` class to automate this
process. This class can also read command line arguments to modify how
tests are run and reported. The easiest way to do this is to create the
following `main` function, which calls `Verify.run`, which creates the `Runner`
objects and calls it appropriately.
```vala
int main (string[] args) {
    var my_test = new MyTest ();
    
    return Verify.run (my_test, args);
}
```

### The generated test program
By default, this program behaves
like a normal UNIX program, meaning that if no test results in an error, the
program will print nothing and return 0. If a test case/suite does fail, it
prints a diagnostic, and returns the total number of errors. 

If you would like more verbose output, you can pass `-v` or `--verbose` on the
command line.

### Further details
Test cases are run in separate processes, and will capture standard error
signals such as `SIGABRT` and `SIGSEGV`. These are used to report failures.
`assert` failures are also captured.

