/* Copyright 2022 Kate Wulff <katty.wulff@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

public class TestOutputIsCaptured : Verify.Case {
    private class AuxTest : Verify.Case {
        public static string test_string = "Hello, World!\n";
        public override void test () {
            stdout.printf (test_string);
        }
    }

    public override void test () {
        var aux_test = new AuxTest ();
        var result = aux_test.run () as Verify.CaseResult;

        assert_true (result.out_message == AuxTest.test_string);
    }
}

