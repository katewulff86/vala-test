/* Copyright 2022 Kate Wulff <katty.wulff@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

public class FailCaseTestSuite : Verify.Suite {
    construct {
        add_many ({
            new FailingCaseNonFailureIsFailure (),
            new FailingCaseFailureIsSuccess (),
        });
    }
}

class FailingCaseNonFailureIsFailure : Verify.Case {
    private class AuxTest : Verify.FailCase {
        public override void test () {
            assert_true (true);
        }
    }

    public override void test () {
        var aux_test = new AuxTest ();
        var result = aux_test.run () as Verify.CaseResult;

        assert_false (result.pass);
    }
}

class FailingCaseFailureIsSuccess : Verify.Case {
    private class AuxTest : Verify.FailCase {
        public override void test () {
            assert_true (false);
        }
    }

    public override void test () {
        var aux_test = new AuxTest ();
        var result = aux_test.run () as Verify.CaseResult;

        assert_true (result.pass);
    }
}

