/* Copyright 2022 Kate Wulff <katty.wulff@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

public class AssertTestSuite : Verify.Suite {
    construct {
        add_many ({
            new AssertTest (),
            new AssertFailTest (),
            new AssertFalseTest (),
            new AssertFalseFailTest (),
            new IntEqTest (),
            new IntEqFailTest (),
            new IntNeTest (),
            new IntNeFailTest (),
            new IntGtTest (),
            new IntGtFailTest (),
            new IntGeTest (),
            new IntGeFailTest (),
            new IntLtTest (),
            new IntLtFailTest (),
            new IntLeTest (),
            new IntLeFailTest (),
            new FltEqTest (),
            new FltEqFailTest (),
            new FltNeTest (),
            new FltNeFailTest (),
            new FltGtTest (),
            new FltGtFailTest (),
            new FltGeTest (),
            new FltGeFailTest (),
            new FltLtTest (),
            new FltLtFailTest (),
            new FltLeTest (),
            new FltLeFailTest (),
        });
    }
}

class AssertTest : Verify.Case {
    public override void test () {
        Verify.assert (true);
    }
}

class AssertFailTest : Verify.FailCase {
    public override void test () {
        Verify.assert (false);
    }
}

class AssertFalseTest : Verify.Case {
    public override void test () {
        Verify.assert_false (false);
    }
}

class AssertFalseFailTest : Verify.FailCase {
    public override void test () {
        Verify.assert_false (true);
    }
}

class IntEqTest : Verify.Case {
    public override void test () {
        Verify.assert_int_eq (5, 5);
    }
}

class IntEqFailTest : Verify.FailCase {
    public override void test () {
        Verify.assert_int_eq (5, 7);
    }
}

class IntNeTest : Verify.Case {
    public override void test () {
        Verify.assert_int_ne (5, 7);
    }
}

class IntNeFailTest : Verify.FailCase {
    public override void test () {
        Verify.assert_int_ne (5, 5);
    }
}

class IntGtTest : Verify.Case {
    public override void test () {
        Verify.assert_int_gt (7, 5);
    }
}

class IntGtFailTest : Verify.FailCase {
    public override void test () {
        Verify.assert_int_gt (7, 7);
        Verify.assert_int_gt (5, 7);
    }
}

class IntGeTest : Verify.Case {
    public override void test () {
        Verify.assert_int_ge (7, 5);
        Verify.assert_int_ge (7, 7);
    }
}

class IntGeFailTest : Verify.FailCase {
    public override void test () {
        Verify.assert_int_ge (5, 7);
    }
}

class IntLtTest : Verify.Case {
    public override void test () {
        Verify.assert_int_lt (5, 7);
    }
}

class IntLtFailTest : Verify.FailCase {
    public override void test () {
        Verify.assert_int_lt (5, 5);
        Verify.assert_int_lt (7, 5);
    }
}

class IntLeTest : Verify.Case {
    public override void test () {
        Verify.assert_int_le (5, 7);
        Verify.assert_int_le (7, 7);
    }
}

class IntLeFailTest : Verify.FailCase {
    public override void test () {
        Verify.assert_int_le (7, 5);
    }
}

class FltEqTest : Verify.Case {
    public override void test () {
        Verify.assert_flt_eq (5.0, 5.0);
        Verify.assert_flt_eq (5.0, 5.000000001, 1e-9);
    }
}

class FltEqFailTest : Verify.FailCase {
    public override void test () {
        Verify.assert_flt_eq (5.0, 5.001);
        Verify.assert_flt_eq (5.0, 5.000000001, 1e-6);
    }
}

class FltNeTest : Verify.Case {
    public override void test () {
        Verify.assert_flt_ne (5.0, 6.0);
        Verify.assert_flt_ne (5.0, 5.000000001, 1e-10);
    }
}

class FltNeFailTest : Verify.FailCase {
    public override void test () {
        Verify.assert_flt_ne (5.0, 5.0);
        Verify.assert_flt_ne (5.0, 5.000000001, 1e-9);
    }
}

class FltGtTest : Verify.Case {
    public override void test () {
        Verify.assert_flt_gt (6.0, 5.0);
        Verify.assert_flt_gt (5.000000001, 5.0, 1e-10);
    }
}

class FltGtFailTest : Verify.FailCase {
    public override void test () {
        Verify.assert_flt_gt (5.0, 5.0);
        Verify.assert_flt_gt (5.0, 6.0);
        Verify.assert_flt_gt (5.000000001, 5.0, 1e-9);
    }
}

class FltGeTest : Verify.Case {
    public override void test () {
        Verify.assert_flt_ge (6.0, 5.0);
        Verify.assert_flt_ge (5.0, 5.0);
        Verify.assert_flt_ge (5.0, 5.000000001, 1e-9);
    }
}

class FltGeFailTest : Verify.FailCase {
    public override void test () {
        Verify.assert_flt_ge (5.0, 6.0);
        Verify.assert_flt_ge (5.0, 5.000000001, 1e-10);
    }
}

class FltLtTest : Verify.Case {
    public override void test () {
        Verify.assert_flt_lt (5.0, 6.0);
        Verify.assert_flt_lt (5.0, 5.000000001, 1e-10);
    }
}

class FltLtFailTest : Verify.FailCase {
    public override void test () {
        Verify.assert_flt_lt (6.0, 5.0);
        Verify.assert_flt_lt (5.0, 5.0);
        Verify.assert_flt_lt (5.0, 5.000000001, 1e-9);
    }
}

class FltLeTest : Verify.Case {
    public override void test () {
        Verify.assert_flt_le (5.0, 6.0);
        Verify.assert_flt_le (5.0, 5.0);
        Verify.assert_flt_le (5.000000001, 5.0, 1e-9);
    }
}

class FltLeFailTest : Verify.FailCase {
    public override void test () {
        Verify.assert_flt_le (6.0, 5.0);
        Verify.assert_flt_le (5.000000001, 5.0, 1e-10);
    }
}

