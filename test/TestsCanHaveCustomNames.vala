/* Copyright 2022 Kate Wulff <katty.wulff@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

public class TestsCanHaveCustomNames : Verify.Case {
    private class AuxTest : Verify.Case {
        public override string name () {
            return "hello";
        }

        public override void test () {
        }
    }

    public override void test () {
        var aux_test = new AuxTest ();
        Verify.assert (aux_test.name () == "hello");
    }

    public override string name () {
        return "CustomNameTest";
    }
}

