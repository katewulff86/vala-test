/* Copyright 2022 Kate Wulff <katty.wulff@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

public class FixtureTestSuite : Verify.Suite {
    construct {
        add_many ({
            new FixtureSetupIsCalled (),
            new FixtureTeardownIsCalled (),
            new FixtureSetupIsCalledBeforeTest (),
            new FixtureTeardownIsCalledAfterTest (),
        });
    }
}

class FixtureSetupIsCalled : Verify.Case {
    private class AuxTest : Verify.Case {
        public override void setup () {
            stdout.printf ("called\n");
        }
    }

    public override void test () {
        var aux_test = new AuxTest ();
        var result = aux_test.run () as Verify.CaseResult;

        Verify.assert (result.out_message == "called\n");
    }
}

class FixtureTeardownIsCalled : Verify.Case {
    private class AuxTest : Verify.Case {
        public override void teardown () {
            stdout.printf ("called\n");
        }
    }

    public override void test () {
        var aux_test = new AuxTest ();
        var result = aux_test.run () as Verify.CaseResult;

        Verify.assert (result.out_message == "called\n");
    }
}

class FixtureSetupIsCalledBeforeTest : Verify.Case {
    private class AuxTest : Verify.Case {
        public bool setup_called = false;

        public override void setup () {
            setup_called = true;
        }

        public override void test () {
            Verify.assert (setup_called);
        }
    }

    public override void test () {
        var aux_test = new AuxTest ();
        aux_test.run ();
    }
}

class FixtureTeardownIsCalledAfterTest : Verify.Case {
    private class AuxTest : Verify.Case {
        public bool teardown_called = false;

        public override void teardown () {
            teardown_called = true;
        }

        public override void test () {
            Verify.assert_false (teardown_called);
        }
    }

    public override void test () {
        var aux_test = new AuxTest ();
        aux_test.run ();
    }
}

