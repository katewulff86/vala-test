/* Copyright 2022 Kate Wulff <katty.wulff@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

public class SelfTestSuite : Verify.Suite {
    construct {
        add_many ({
            new SuccessfulCaseIsSuccessful (),
            new FailingCaseIsFailure (),
            new FailCaseTestSuite (),
            new AssertTestSuite (),
            new FixtureTestSuite (),
            new TestsCanHaveCustomNames (),
            new TestOutputIsCaptured (),
        });
    }
}

