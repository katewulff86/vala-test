/* Copyright 2022 Kate Wulff <katty.wulff@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

public class SuccessfulCaseIsSuccessful : Verify.Case {
    private class AuxTest : Verify.Case {
        public override void test () {
        }
    }

    public override void test () {
        var aux_test = new AuxTest ();
        var result = aux_test.run () as Verify.CaseResult;

        assert_true (result.pass);
    }
}

