/* Copyright 2022 Kate Wulff <katty.wulff@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

public static int main (string[] args) {
    var test_suite = new SelfTestSuite ();

    return Verify.run (test_suite, args);
}

