/* Copyright 2022 Kate Wulff <katty.wulff@gmail.com>
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

namespace Verify {
    public class Result : Object {
        public Test test;
        public double time;
        public bool pass;
    }

    public class CaseResult : Result {
        public string out_message;
        public string err_message;
    }

    public class SuiteResult : Result {
        public Gee.List<Result?> results;
    }
}

