/* Copyright 2022 Kate Wulff <katty.wulff@gmail.com>
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

namespace Verify {
    public interface Reporter : Object {
        public abstract int report (Result result, ResultHierarchy? hierarchy = null);
    }
}

