/* Copyright 2022 Kate Wulff <katty.wulff@gmail.com>
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

namespace Verify {
    public class ResultHierarchy : Object {
        public Result result;
        public ResultHierarchy? parent;

        public ResultHierarchy (Result result, ResultHierarchy? parent = null) {
            this.result = result;
            this.parent = parent;
        }

        public string to_string () {
            var test_name = result.test.name ();

            if (parent == null)
                return test_name;
            else
                return @"$(parent.to_string ())/$(test_name)";
        }
    }
}

