/* Copyright 2022 Kate Wulff <katty.wulff@gmail.com>
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

namespace Verify {
    public interface Test : Object {
        public delegate void TimeableFunc ();
        public abstract Result run ();
        public virtual double time (TimeableFunc func) {
            var timer = new Timer ();
            timer.start ();
            func ();
            timer.stop ();

            return timer.elapsed ();
        }
        public abstract string name ();
    }
}

