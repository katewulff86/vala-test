/* Copyright 2022 Kate Wulff <katty.wulff@gmail.com>
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

namespace Verify {
    public int run (Test test, string[] args) {
        try {
            var runner = new Runner (args);

            return runner.run (test);
        } catch (OptionError e) {
            stderr.printf ("error: %s\n", e.message);
            stderr.printf ("Run '%s --help' to see a full list of available command line options.\n", args[0]);

            return Posix.EXIT_FAILURE;
        }
    }
}

