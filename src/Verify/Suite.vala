/* Copyright 2022 Kate Wulff <katty.wulff@gmail.com>
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

namespace Verify {
    public class Suite : Object, Test {
        protected Gee.List<Test?> tests;

        construct {
            tests = new Gee.ArrayList<Test?> ();
        }

        public virtual Result run () {
            var result = new SuiteResult ();
            result.test = this;
            result.pass = true;
            result.time = time (() => {
                result.results = run_tests (out result.pass);
            });

            return result;
        }

        public virtual string name () {
            return this.get_type ().name ();
        }

        protected void add (Test test) {
            tests.add (test);
        }

        protected void add_many (Test[] tests) {
            foreach (var test in tests)
                add (test);
        }

        protected Gee.List<Result?> run_tests (out bool pass) {
            pass = true;
            var results = new Gee.ArrayList<Result?> ();
            foreach (var test in tests) {
                var result = test.run ();
                results.add (result);
                if (!result.pass)
                    pass = false;
            }

            return results;
        }
    }
}

