/* Copyright 2022 Kate Wulff <katty.wulff@gmail.com>
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

namespace Verify {
    public class DefaultReporter : Object, Reporter {
        public bool verbose { get; set; }

        public int report (Result result, ResultHierarchy? hierarchy = null) {
            if (result is SuiteResult)
                return report_suite (result as SuiteResult, hierarchy);
            else if (result is CaseResult)
                return report_case (result as CaseResult, hierarchy);
            else
                stderr.printf ("Result type not recognised.\n");

            return 1;
        }

        public int report_suite (SuiteResult result, ResultHierarchy? parent = null) {
            var hierarchy = new ResultHierarchy (result, parent);

            var num_errors = 0;
            foreach (var subresult in result.results)
                num_errors += report (subresult, hierarchy);

            if (verbose || (num_errors > 0)) {
                stdout.printf ("%s: %d error(s) in %f seconds.\n",
                               hierarchy.to_string (), num_errors, result.time);
            }

            return num_errors;
        }

        public int report_case (CaseResult result, ResultHierarchy? parent = null) {
            var hierarchy = new ResultHierarchy (result, parent);

            if (!result.pass) {
                stdout.printf ("%s: error in %f seconds.\n",
                               hierarchy.to_string (), result.time);
                stdout.printf ("%s", result.out_message);
                stderr.printf ("%s", result.err_message);
            } else if (verbose) {
                stdout.printf ("%s: no error in %f seconds.\n",
                               hierarchy.to_string (), result.time);
            }

            return result.pass ? 0 : 1;
        }
    }
}

