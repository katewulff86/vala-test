/* Copyright 2022 Kate Wulff <katty.wulff@gmail.com>
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

namespace Verify {
    public class Runner : Object {
        private static string? format = null;
        private static bool verbose = false;
        public Reporter reporter { get; private set; }

        private const OptionEntry[] options = {
            {"format", 'f', OptionFlags.NONE, OptionArg.STRING, ref format, "Format of output", "FORMAT"},
            {"verbose", 'v', OptionFlags.NONE, OptionArg.NONE, ref verbose, "Verbose output", null},
            {null},
        };

        public Runner (string[] args) throws OptionError {
            format = null;
            verbose = false;

            var context = new OptionContext ("vala-test");
            context.set_help_enabled (true);
            context.add_main_entries (options, null);
            context.parse (ref args);

            switch (format) {
                case null:
                case "default":
                    var default_reporter = new DefaultReporter ();
                    default_reporter.verbose = verbose;
                    reporter = default_reporter;
                    break;
                default:
                    throw new OptionError.BAD_VALUE ("unknown format: %s".printf (format));
            }
        }

        public int run (Test test) {
            var result = test.run ();

            return reporter.report (result);
        }
    }
}

