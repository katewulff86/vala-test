/* Copyright 2022 Kate Wulff <katty.wulff@gmail.com>
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

namespace Verify {
    public double DEFAULT_EPSILON = 1e-6;

    public void assert (bool expression, string message = "") {
        if (!expression) {
            if (message != "")
                stderr.printf ("assertion failed: %s\n", message);
            else
                stderr.printf ("assertion failed\n");
            Process.abort ();
        }
    }

    public void assert_false (bool expression, string message = "") {
        assert (!expression, message);
    }

    public void assert_int_eq (int a, int b) {
        assert (a == b, @"$a == $b");
    }

    public void assert_int_ne (int a, int b) {
        assert (a != b, @"$a != $b");
    }

    public void assert_int_gt (int a, int b) {
        assert (a > b, @"$a > $b");
    }

    public void assert_int_ge (int a, int b) {
        assert (a >= b, @"$a >= $b");
    }

    public void assert_int_lt (int a, int b) {
        assert (a < b, @"$a < $b");
    }

    public void assert_int_le (int a, int b) {
        assert (a <= b, @"$a <= $b");
    }

    private bool float_equal (double a, double b, double epsilon) {
        var abs_diff = (a - b).abs ();
        var max = double.max (a.abs (), b.abs ());
        var rel = epsilon * max;

        return abs_diff <= rel;
    }

    public void assert_flt_eq (double a, double b, double epsilon = DEFAULT_EPSILON) {
        assert (float_equal (a, b, epsilon), @"$a == $b");
    }

    public void assert_flt_ne (double a, double b, double epsilon = DEFAULT_EPSILON) {
        assert (!float_equal (a, b, epsilon), @"$a != $b");
    }

    public void assert_flt_gt (double a, double b, double epsilon = DEFAULT_EPSILON) {
        assert (a > b && !float_equal (a, b, epsilon), @"$a > $b");
    }

    public void assert_flt_ge (double a, double b, double epsilon = DEFAULT_EPSILON) {
        assert (a > b || float_equal (a, b, epsilon), @"$a >= $b");
    }

    public void assert_flt_lt (double a, double b, double epsilon = DEFAULT_EPSILON) {
        assert (a < b || float_equal (a, b, epsilon), @"$a < $b");
    }

    public void assert_flt_le (double a, double b, double epsilon = DEFAULT_EPSILON) {
        assert (a <= b || float_equal (a, b, epsilon), @"$a <= $b");
    }
}

