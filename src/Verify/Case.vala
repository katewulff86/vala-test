/* Copyright 2022 Kate Wulff <katty.wulff@gmail.com>
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

namespace Verify {
    public class Case : Object, Test {
        protected int out_fd[2];
        protected int err_fd[2];

        public virtual void test () {}
        public virtual void setup () {}
        public virtual void teardown () {}

        public virtual Result run () {
            var result = new CaseResult ();
            var t = time (() => {
                result = run_in_new_process ();
            });
            result.time = t;

            return result;
        }

        public virtual string name () {
            return this.get_type ().name ();
        }

        protected CaseResult run_in_new_process () {
            Posix.pipe (out_fd);
            Posix.pipe (err_fd);

            var pid = Posix.fork ();
            if (pid == 0) {
                // Child process
                close_read_pipes ();
                run_test ();
                close_write_pipes ();
                Process.exit (Posix.EXIT_SUCCESS);
            } else {
                // Parent process
                close_write_pipes ();
                var result = retrieve_result (pid);
                close_read_pipes ();

                return result;
            }
        }

        protected void close_read_pipes () {
            Posix.close (out_fd[0]);
            Posix.close (err_fd[0]);
        }

        protected void close_write_pipes () {
            Posix.close (out_fd[1]);
            Posix.close (err_fd[1]);
        }

        protected void run_test () {
            setup_signal_handlers ();
            redirect_output ();

            setup ();
            test ();
            teardown ();
        }

        protected void redirect_output () {
            Posix.dup2 (out_fd[1], 1);
            Posix.dup2 (err_fd[1], 2);
        }

        protected void setup_signal_handlers () {
            Posix.signal (Posix.Signal.ABRT, signal_handler);
            Posix.signal (Posix.Signal.TERM, signal_handler);
            Posix.signal (Posix.Signal.SEGV, signal_handler);
            Posix.signal (Posix.Signal.ILL, signal_handler);
            Posix.signal (Posix.Signal.FPE, signal_handler);
        }

        protected static void signal_handler (int signal) {
            stderr.printf ("Program received %s (%i).\n",
                           signal_name (signal),
                           signal);
            Posix.exit (Posix.EXIT_FAILURE);
        }

        protected static unowned string signal_name (int signal) {
            switch (signal) {
            case Posix.Signal.ABRT:
                return "SIGABRT";
            case Posix.Signal.TERM:
                return "SIGTERM";
            case Posix.Signal.SEGV:
                return "SIGSEGV";
            case Posix.Signal.ILL:
                return "SIGILL";
            case Posix.Signal.FPE:
                return "SIGFPE";
            default:
                return "unknown signal";
            }
        }

        protected CaseResult retrieve_result (Posix.pid_t pid) {
            int status;
            Posix.waitpid (pid, out status, 0);

            var result = new CaseResult ();
            result.test = this;
            result.out_message = capture_output (out_fd[0]);
            result.err_message = capture_output (err_fd[0]);

            if (Process.if_exited (status))
                result.pass = Process.exit_status (status) == 0;
            else
                result.pass = false;

            return result;
        }

        protected string capture_output (int fd) {
            var file = Posix.FILE.fdopen (fd, "r");
            var output = read_all (file);
            file.close ();

            return output;
        }

        protected string read_all (Posix.FILE file) {
            var result = new char[8];
            size_t length = 0;
            size_t capacity = 8;

            while (!file.eof ()) {
                var bytes_to_read = capacity - length;
                var bytes_read = file.read (&result[length],
                                            sizeof (char),
                                            bytes_to_read);
                length += bytes_read;
                if (length == capacity) {
                    capacity *= 2;
                    result.resize ((int) capacity);
                }
            }

            return (string) result;
        }
    }
}

