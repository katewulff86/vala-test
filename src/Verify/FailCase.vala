/* Copyright 2022 Kate Wulff <katty.wulff@gmail.com>
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

namespace Verify {
    public class FailCase : Case {
        public override Result run () {
            var result = base.run ();
            result.pass = !result.pass;

            return result;
        }
    }
}

